require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// Establish Connection to database
mongoose.connect("mongodb://admin:admin@ac-us6slip-shard-00-00.jza2gcm.mongodb.net:27017,ac-us6slip-shard-00-01.jza2gcm.mongodb.net:27017,ac-us6slip-shard-00-02.jza2gcm.mongodb.net:27017/?ssl=true&replicaSet=atlas-6mpk90-shard-0&authSource=admin&retryWrites=true&w=majority",{

	useNewUrlParser: true,
	useUnifiedTopology: true

});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Failed connecting to database."));
db.once("open", () => console.log("Successfully connected to database"));

//ROUTES BELOW

// REQUIRE THE ROUTES ******************
const authRoutes = require('./routes/authRoutes');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');

// SETUP ROUTES ******************
app.use('/', authRoutes);
app.use('/user', userRoutes);
app.use('/product', productRoutes);

//ROUTES ABOVE

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Port running at ${port}`));